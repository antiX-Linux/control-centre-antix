��    V      �     |      x     y     �     �     �     �     �     �     �               '     >     V     g     x     �     �     �     �  "   �     	     	     	     !	     7	     L	     ^	     q	     �	     �	     �	     �	     �	     �	     �	     
     
     1
     6
     K
     `
     x
     �
     �
     �
     �
     �
     �
     �
     �
          &     5     G     Y     s     �     �     �     �     �     �     �                !     /     N     b     �     �     �     �     �     �     �     �  
             &     ;     H     e     |     �    �  $   �     �     �     �          ,     G  !   b  +   �     �     �  &   �              *   8  @   c     �  $   �     �  .   �     )     ?     L     d  .   �     �  (   �     �          8  $   T     y  
   �  $   �     �  .   �       
   .  !   9  8   [  .   �     �     �     �        "        7     M     T     h  7   �     �      �      �          2     P  6   h     �     �  5   �     �  
     )        C     b  >   }     �  0   �  /     @   8     y     �     �     �  
   �     �     �  #   �  /   !     Q     i  ,   �     �     �            M   	          ,             +         3              "              8   <              >   H           #   O      (          N   B   =   U         *       ?              %   K       2       E       I   0   P       -              Q          D       4       @   6            /               $             )       L   T             &   9       S   
   ;       5       '      C   V                      G          !      R   .              :   F           A   7   J   1     DisconnectShares 1-to-1 Assistance 1-to-1 Voice ADSL/PPPOE Configuration Adblock Adjust Mixer Alsamixer Equalizer Alternatives Configurator Backlight Brightness Boot Repair Change Slim Background Choose Startup Services Choose Wallpaper Codecs Installer Configure Automount Configure Live Persistence ConnectShares Configuration Customize Look and Feel Desktop Dial-Up Configuaration (GNOME PPP) Disk Manager Disks Drivers Droopy (File Sharing) Edit Bootloader Menu Edit Config Files Edit Exclude Files Edit Fluxbox Settings Edit IceWM Settings Edit JWM Settings Edit System Monitor (Conky) Firewall Configuration Hardware ISO Snapshot IceWM Control Centre Image a Partition Install antiX Linux Live Live USB Maker (cli) Live USB Maker (gui) Live-USB Kernel Updater Login Manager Maintenance Manage Packages Menu Editor Mount Connected Devices Mouse Configuration Network Network Assistant Network Interfaces (Ceni) Nvidia Driver Installer PC Information Package Installer Partition a Drive Password Prompt (su/sudo) Preferred Applications Print Settings Remaster-Customize Live Repo Manager SSH Conduit Save Root Persistence Select wifi Application Session Set Auto-Login Set Date and Time Set Font Size Set Grub Boot Image (png only) Set Screen Blanking Set Screen Resolution (ARandR) Set System Keyboard Layout Set Up Live Persistence Shares Software Sound Card Chooser Synchronize Directories System System Backup Test Sound Toggle Firewall on/off User Desktop-Session User Manager WPA Supplicant Configuration WiFi Connect (Connman) antiX Updater antiX autoremove Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2011-10-27 11:08+0000
Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>, 2023
Language-Team: Hebrew (Israel) (http://app.transifex.com/anticapitalista/antix-development/language/he_IL/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: he_IL
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n == 2 && n % 1 == 0) ? 1: (n % 10 == 0 && n % 1 == 0 && n > 10) ? 2 : 3;
 DisconnectShares (ניתוק Shares) תמיכה אחד-על-אחד קול אחד-על-אחד הגדרות ADSL/PPPOE חסימת פרסומות כוון מערבל קול אקולייזר Alsamixer מגדיר האלטרנטיבות בהירות תאורת מסך אחורית תיקון אתחול שנה רקע Slim בחירת שירותים בהפעלה נא לבחור רקע אשף התקנת הקודקים הגדר הצבה-אוטומטית (mount) הגדר התקיימות ב USB להתקנה (Live Persistence) הגדרות ConnectShares התאמת המראה והתחושה שולחן עבודה הגדרות חיבור חייגן (GNOME PPP) מנהל כוננים כוננים מנהלי התקנים Droopy (שיתוף קבצים) ערוך את תפריט מנהל האתחול ערוך קבצי הגדרות ערוך את קבצי האי הכללה עריכת הגדרות Fluxbox עריכת הגדרות IceWM עריכת הגדרות JWM ערוך מנטר מערכת (Conky) הגדרת חומת האש חומרה ISO Snapshot (תמונת-גיבוי) מרכז הבקרה של IceWM צור תמונת-גיבוי של המחיצה התקן לינוקס antiX המחשה מייצר USB להתקנה (cli) אשף יצירת USB להמחשה (ממשק משתמש) מעדכן הליבה של מצב ההמחשה מנהל התחברות תחזוקה ניהול חבילות עורך תפריט הצב התקנים מחוברים הגדרת העכבר רשת מסייע הרשת התקני רשת (Ceni) אשף התקנת מנהלי ההתקנים של Nvidia מידע על המחשב אשף התקנת החבילות הגדר מחיצות בכונן הנחה סיסמא (su/sudo) יישומים מועדפים הגדרות הדפסה Remaster - התאמה אישית של USB להתקנה מנהל מאגרים SSH Conduit שמור התקיימות שורש (Root Persistance) בחר תוכנת wifi הפעלה הגדר התחברות-אוטומטית. הגדרת תאריך ושעה הגדר גודל גופן הגדר תמונת אתחול ב Grub (קובץ png בלבד) הגדר כיבוי מסך קבע את רזולוציית המסך (ARandR) קביעת פריסת המקלדת במערכת התקן התקיימות ב USB להתקנה (Live Persistence) שיתופים תוכנות בחירת כרטיס קול סנכרן תיקיות מערכת גיבוי מערכת בדיקת שמע כיבוי/הפעלת חומת אש פעילות-שולחן עבודה למשתמש מנהל משתמשים הגדרות  WPA Supplicant חיבור לרשת אלחוטית (Connman) מעדכן antiX הסרה-אוטומטית antiX 