control-centre-antix (1.1.8) unstable; urgency=medium

  * use correct icon path for antiX-samba manager

 -- anticapitalista <antix@operamail.com>  Sun, 03 Mar 2024 16:42:40 +0200

control-centre-antix (1.1.7) unstable; urgency=medium

  * fix for user closing installer window prematurely by X in window border
  * added multilingual keyboard 4 level layout map
  * thanks Robin
  * translation update

 -- anticapitalista <antix@operamail.com>  Fri, 01 Mar 2024 17:28:47 +0200

control-centre-antix (1.1.6) unstable; urgency=medium

  * integrate antiX Samba manager - thanks @Robin

 -- anticapitalista <antix@operamail.com>  Tue, 20 Feb 2024 16:07:13 +0200

control-centre-antix (1.1.5) unstable; urgency=medium

  * Fix duplicate icon-name=
  * needed icons 48x48 png format added to papirus, numix-square and numix-bevel subfolders of icons folder.
  * update translations

 -- anticapitalista <antix@operamail.com>  Fri, 09 Feb 2024 12:09:47 +0200

control-centre-antix (1.1.4) unstable; urgency=medium

  * fixed typos
  * added Shared Folders,  added antiX cloud, added Language
  * put suspend-grey icon in /usr/share/pixmaps/papirus

 -- anticapitalista <antix@operamail.com>  Mon, 05 Feb 2024 14:00:15 +0200

control-centre-antix (1.1.3) unstable; urgency=medium

  * use rxvt for htop and alsamixer
  * added entry to script file itself.
  * updated .pot file with new entry.    
  * added png 48 used for this entry (sheets.png), expected to live in $ICONS
  * Improved handling of startup file entries.
  * Missing Ampersand fixed in process management entry
  * Fix typo "snatpshot" → "snapshot"
  * Added autosuspend setup and management entry. Needs suspend_if_idle script from yad-goodies-antix to be updated.
  * Missing tooltips in live tab added
  * Some tooltips improved to avoid redundant pieces of information.
  * Some line breaks added where applicable, to improve readability of long lines.
  * Thanks Robin

 -- anticapitalista <antix@operamail.com>  Mon, 29 Jan 2024 12:05:33 +0200

control-centre-antix (1.1.2) unstable; urgency=medium

  * added png icons
  * add xdotool and wmctrl to the dependencies
  * Added functions for adding/removing antiX startup file entries.
  * Improved plain ALSA ./. pipewire sound server layer logic.
  * Added clipit switch incluing taking care for startup file.
  * use urxvt for alsamixer
  * Many thanks to Robin

 -- anticapitalista <antix@operamail.com>  Fri, 26 Jan 2024 17:50:29 +0200

control-centre-antix (1.1.1) unstable; urgency=medium

  * bugfix in test condition for equaliser entry
  * updated translations

 -- anticapitalista <antix@operamail.com>  Tue, 26 Dec 2023 20:42:19 +0200

control-centre-antix (1.1.0) unstable; urgency=medium

  * added more tooltips
  * bump version

 -- anticapitalista <antix@operamail.com>  Mon, 13 Nov 2023 19:33:06 +0200

control-centre-antix (1.0.9) unstable; urgency=medium

  * add pipewire_toggle to hardware

 -- anticapitalista <antix@operamail.com>  Sun, 29 Oct 2023 15:54:18 +0200

control-centre-antix (1.0.8) unstable; urgency=medium

  * use desktop file to detect alsamixer-equaliser

 -- anticapitalista <antix@operamail.com>  Mon, 02 Oct 2023 17:44:42 +0300

control-centre-antix (1.0.7) unstable; urgency=medium

  * include translations for tooltips

 -- anticapitalista <antix@operamail.com>  Fri, 04 Aug 2023 16:00:14 +0300

control-centre-antix (1.0.6) unstable; urgency=medium

  * desktop files in usr/share/applications

 -- anticapitalista <antix@operamail.com>  Mon, 03 Jul 2023 19:06:47 +0300

control-centre-antix (1.0.5) unstable; urgency=medium

  * update translations
  * tooltips added- thanks to Robin

 -- anticapitalista <antix@operamail.com>  Sun, 28 May 2023 16:01:00 +0300

control-centre-antix (1.0.4) unstable; urgency=medium

  * add and fix edit icewm file options - thanks BobC 

 -- anticapitalista <antix@operamail.com>  Sat, 13 May 2023 17:10:12 +0300

control-centre-antix (1.0.3) unstable; urgency=medium

  * updated translations

 -- anticapitalista <antix@operamail.com>  Fri, 28 Apr 2023 20:03:01 +0300

control-centre-antix (1.0.2) unstable; urgency=medium

  * update translations
  * rejuggle entries - thanks PPC

 -- anticapitalista <antix@operamail.com>  Wed, 22 Mar 2023 14:18:44 +0200

control-centre-antix (1.0.1) unstable; urgency=medium

  * update translations

 -- anticapitalista <antix@operamail.com>  Mon, 13 Mar 2023 20:12:17 +0200

control-centre-antix (1.0.0) unstable; urgency=medium

  * include antix_firewall_toggle
  * bump version for antiX-23

 -- anticapitalista <antix@operamail.com>  Thu, 23 Feb 2023 15:14:18 +0200

control-centre-antix (0.8.15) unstable; urgency=medium

  * fix new path of antiX User manager fom /usr/sbin to usr/bin

 -- anticapitalista <antix@operamail.com>  Thu, 09 Feb 2023 17:32:20 +0200

control-centre-antix (0.8.14) unstable; urgency=medium

  * include GenericName in desktop file
  * updated translations

 -- anticapitalista <antix@operamail.com>  Tue, 10 Jan 2023 00:10:05 +0200

control-centre-antix (0.8.13) unstable; urgency=medium

  * added hu and nl_BE

 -- anticapitalista <antix@operamail.com>  Mon, 26 Sep 2022 16:40:09 +0300

control-centre-antix (0.8.12) unstable; urgency=medium

  * updated desktop file

 -- anticapitalista <antix@operamail.com>  Sun, 14 Aug 2022 11:59:42 +0300

control-centre-antix (0.8.11) unstable; urgency=medium

  * further tanslation updates

 -- anticapitalista <antix@operamail.com>  Mon, 20 Jun 2022 15:08:52 +0300

control-centre-antix (0.8.10) unstable; urgency=medium

  * translatio updates

 -- anticapitalista <antix@operamail.com>  Tue, 12 Oct 2021 16:16:29 +0300

control-centre-antix (0.8.9) unstable; urgency=medium

  * ddm-mx path changed to /usr/bin
  * updated localisation

 -- anticapitalista <antix@operamail.com>  Fri, 24 Sep 2021 16:50:34 +0300

control-centre-antix (0.8.8) unstable; urgency=medium

  * added a check for runit in rc-conf-wrapper.sh

 -- anticapitalista <antix@operamail.com>  Thu, 07 Sep 2021 14:32:43 +0300

control-centre-antix (0.8.7) unstable; urgency=medium

  * added runit-service-manager

 -- anticapitalista <antix@operamail.com>  Thu, 02 Sep 2021 14:32:43 +0300

control-centre-antix (0.8.6) unstable; urgency=medium

  * updated desktop file

 -- anticapitalista <antix@operamail.com>  Sun, 29 Aug 2021 13:14:16 +0300

control-centre-antix (0.8.5) unstable; urgency=medium

  * added slimski and zzzfm support

 -- anticapitalista <antix@operamail.com>  Sun, 18 Jul 2021 18:21:36 +0300

control-centre-antix (0.8.4) unstable; urgency=medium

  * fix codecs entry
  * gksu galternatives
  * removed gksu-properties

 -- anticapitalista <antix@operamail.com>  Sun, 02 May 2021 18:38:49 +0300

control-centre-antix (0.8.3) unstable; urgency=medium

  * added antiX autoremove

 -- anticapitalista <antix@operamail.com>  Mon, 08 Feb 2021 12:34:17 +0200

control-centre-antix (0.8.2) unstable; urgency=medium

  * added Software category
  * added some more icons

 -- anticapitalista <antix@operamail.com>  Fri, 29 Jan 2021 15:17:03 +0200

control-centre-antix (0.8.1) unstable; urgency=medium

  * corrected vaious recommended dependencies
  * re-ordered some of the icon entries

 -- anticapitalista <antix@operamail.com>  Mon, 04 Jan 2021 15:53:25 +0200

control-centre-antix (0.8.0) unstable; urgency=medium

  * various bullseye changes

 -- anticapitalista <antix@operamail.com>  Tue, 22 Dec 2020 14:57:41 +0200

control-centre-antix (0.7.26) unstable; urgency=medium

  * wallpaper app calles /usr/local/bin/wallpaper not wallpaper.py

 -- anticapitalista <antix@operamail.com>  Sat, 12 Dec 2020 18:23:53 +0200

control-centre-antix (0.7.25) unstable; urgency=medium

  * Added gl_ES

 -- anticapitalista <antix@operamail.com>  Fri, 27 Nov 2020 20:38:54 +0200

control-centre-antix (0.7.24) unstable; urgency=medium

  * improved pt_BR, tr, zh_CN added nb and th

 -- anticapitalista <antix@operamail.com>  Thu, 08 Oct 2020 13:42:31 +0300

control-centre-antix (0.7.23) unstable; urgency=medium

  * added zh languages

 -- anticapitalista <antix@operamail.com>  Wed, 27 May 2020 16:38:46 +0300

control-centre-antix (0.7.22) unstable; urgency=medium

  * added connman-ui-gtk - on antiX-base editions

 -- anticapitalista <antix@operamail.com>  Fri, 10 Apr 2020 12:05:58 +0300

control-centre-antix (0.7.21) unstable; urgency=medium

  * added fi and ja translations

 -- anticapitalista <antix@operamail.com>  Mon, 23 Mar 2020 13:07:02 +0200

control-centre-antix (0.7.20) unstable; urgency=medium

  * ceni moved to /usr/sbin following Debian upstream

 -- anticapitalista <antix@operamail.com>  Thu, 13 Feb 2020 19:30:03 +0200

control-centre-antix (0.7.19) unstable; urgency=medium

  * added disk-manager
  * all of the predefined notebook tabs are now displayed, even if none of the items slated for a given tab is currently installed - thanks skidoo

 -- anticapitalista <antix@operamail.com>  Sun, 22 Dec 2019 13:56:24 +0200

control-centre-antix (0.7.18) unstable; urgency=medium

  * sysvrc.conf no longer a depends - if user installs runit, control centre is not removed

 -- anticapitalista <antix@operamail.com>  Wed, 11 Dec 2019 02:24:01 +0200

control-centre-antix (0.7.17) unstable; urgency=medium

  * updated tanslations

 -- anticapitalista <antix@operamail.com>  Sun, 13 Oct 2019 14:33:57 +0300

control-centre-antix (0.7.16) unstable; urgency=medium

  * added cmst (connman qt gui)

 -- anticapitalista <antix@operamail.com>  Fri, 04 Oct 2019 21:21:22 +0300

control-centre-antix (0.7.15) unstable; urgency=medium

  * new date and time script

 -- anticapitalista <antix@operamail.com>  Sun, 08 Sep 2019 21:09:12 +0100

control-centre-antix (0.7.14) unstable; urgency=medium

  * changed grub background comment to png only

 -- anticapitalista <antix@operamail.com>  Thu, 22 Aug 2019 21:01:35 +0100

control-centre-antix (0.7.13) unstable; urgency=medium

  * reverted iso-snapshot icon

 -- anticapitalista <antix@operamail.com>  Tue, 20 Aug 2019 21:37:02 +0100

control-centre-antix (0.7.12) unstable; urgency=medium

  * make droopy a dependency to avoid messing up tab options if removed

 -- anticapitalista <antix@operamail.com>  Sat, 17 Aug 2019 10:58:38 +0100

control-centre-antix (0.7.11) unstable; urgency=medium

  * a couple of changed icons, persist-makefs

 -- anticapitalista <antix@operamail.com>  Thu, 01 Aug 2019 22:06:42 +0100

control-centre-antix (0.7.10) unstable; urgency=medium

  * more icon changes - thanks to noClue

 -- anticapitalista <antix@operamail.com>  Tue, 16 Jul 2019 15:52:45 +0100

# Older entries have been removed from this changelog.
# To read the complete changelog use `apt changelog control-centre-antix`.
